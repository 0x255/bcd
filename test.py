# -*- coding: utf-8 -*-

import sys
import pickle

from rpi_methods import Reader
from rpi_methods.baby_cry_predictor import BabyCryPredictor
from rpi_methods.feature_engineer import FeatureEngineer
from rpi_methods.majority_voter import MajorityVoter

# Unpickle the model
with open('model/model.pkl', 'rb') as fp:
    model = pickle.load(fp)

predictor = BabyCryPredictor(model)

if len(sys.argv) < 2:
    print("Usage: python test.py <audio file>")
    sys.exit(1)

file_name = sys.argv[1]  # only one file in the folder

# Read signal
file_reader = Reader(file_name)
play_list = file_reader.read_audio_file()

# Feature extraction
engineer = FeatureEngineer()

play_list_processed = list()

for signal in play_list:
    tmp = engineer.feature_engineer(signal)
    play_list_processed.append(tmp)

predictions = list()

for signal in play_list_processed:
    tmp = predictor.classify(signal)
    predictions.append(tmp)

majority_voter = MajorityVoter(predictions)
majority_vote = majority_voter.vote()

print("Prediction: {}".format(majority_vote))
