# -*- coding: utf-8 -*-

import argparse
import os
import sys
import pickle
import logging

from rpi_methods import Reader
from rpi_methods.baby_cry_predictor import BabyCryPredictor
from rpi_methods.feature_engineer import FeatureEngineer
from rpi_methods.majority_voter import MajorityVoter
from rpi_methods.record import record_audio
from rpi_methods.play import play_audio

parser = argparse.ArgumentParser()
parser.add_argument('--load_path_data',
                    default='%s/recording/' % os.path.dirname(os.path.abspath(__file__)))
parser.add_argument('--load_path_model',
                    default='%s/model/' % os.path.dirname(os.path.abspath(__file__)))
parser.add_argument('--save_path',
                    default='%s/prediction/' % os.path.dirname(os.path.abspath(__file__)))
parser.add_argument('--log_path',
                    default='%s/logs/' % os.path.dirname(os.path.abspath(__file__)))

# Arguments
args = parser.parse_args()
load_path_data = args.load_path_data
load_path_model = args.load_path_model
save_path = args.save_path
log_path = args.log_path

logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s',
                    datefmt='%Y-%m-%d %I:%M:%S %p',
                    filename=os.path.join(log_path, 'logs_run.log'),
                    filemode='w',
                    level=logging.DEBUG)

# Unpickle the model
with open((os.path.join(load_path_model, 'model.pkl')), 'rb') as fp:
    model = pickle.load(fp)

predictor = BabyCryPredictor(model)

file_name = 'signal_9s.wav'  # only one file in the folder


def make_prediction():
    # Read signal
    file_reader = Reader(os.path.join(load_path_data, file_name))
    play_list = file_reader.read_audio_file()

    # Feature extraction
    engineer = FeatureEngineer()

    play_list_processed = list()

    for signal in play_list:
        tmp = engineer.feature_engineer(signal)
        play_list_processed.append(tmp)

    predictions = list()

    for signal in play_list_processed:
        tmp = predictor.classify(signal)
        predictions.append(tmp)

    majority_voter = MajorityVoter(predictions)
    majority_vote = majority_voter.vote()

    return majority_vote


print("Start listening")

while True:
    logging.info("Listening...")
    record_audio(os.path.join(load_path_data, file_name))

    logging.info("Making prediction...")
    prediction = make_prediction()
    print("Prediction: {}".format(prediction))
    logging.info("Prediction: {}".format(prediction))

    if prediction == 1:
        play_audio("ne_ok.wav")
    else:
        play_audio("ok.wav")

    # Save prediction result
    with open(os.path.join(save_path, 'prediction.txt'), 'wb') as text_file:
        text_file.write("{0}".format(prediction))
